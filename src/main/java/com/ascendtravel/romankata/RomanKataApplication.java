package com.ascendtravel.romankata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomanKataApplication {

	public static void main(String[] args) {
		SpringApplication.run(RomanKataApplication.class, args);
	}

}
