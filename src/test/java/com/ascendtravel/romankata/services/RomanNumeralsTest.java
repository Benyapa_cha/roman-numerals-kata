package com.ascendtravel.romankata.services;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class RomanNumeralsTest {

    @Test
    public void one() {
        assertEquals("I", RomanNumerals.arabicToRoman(1));
    }

    @Test
    public void two() {
        assertEquals("II", RomanNumerals.arabicToRoman(2));
    }

    @Test
    public void three() {
        assertEquals("III", RomanNumerals.arabicToRoman(3));
    }

    @Test
    public void four() {
        assertEquals("IV", RomanNumerals.arabicToRoman(4));
    }

    @Test
    public void five() {
        assertEquals("V", RomanNumerals.arabicToRoman(5));
    }

    @Test
    public void six() {
        assertEquals("VI", RomanNumerals.arabicToRoman(6));
    }

    @Test
    public void seven() {
        assertEquals("VII", RomanNumerals.arabicToRoman(7));
    }

    @Test
    public void eight() {
        assertEquals("VIII", RomanNumerals.arabicToRoman(8));
    }

    @Test
    public void nine() {
        assertEquals("IX", RomanNumerals.arabicToRoman(9));
    }

}
